const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin  = require('mini-css-extract-plugin');
const path = require('path');
const package = require('./package.json');

module.exports = {
    mode: 'development',
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        port: 8080,
        open: true,
    },
    entry: [
        './src/app.js'
    ],
    output: {
        filename: package.name + '.components.mim.js',
    },
    resolve: {
        extensions: ['.webpack.js', '.js', '.scss', '.vue'],
        alias: {
            'components': path.join(__dirname, 'src/components'),
            'vue': 'vue/dist/vue.common.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: true,
                        },
                    },
                    'css-loader',
                    'sass-loader'
                ],
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: package.name + '.components.mim.css',
            ignoreOrder: false, 
        })
    ]
}