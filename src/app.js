import Vue from 'vue';
import { store } from './store';
import VueMask from 'v-mask';


import Shelf from './components/Shelf.vue';
import Instagram from './components/Instagram.vue';
import Newsletter from './components/Newsletter.vue';
import Navigation from './components/Navigation.vue';
import Cart from './components/Cart.vue';
import Search from './components/Search.vue';
import Favorites from './components/Favorites.vue';
import Login from './components/Login.vue';
import overlay from './components/Overlay.vue';

import Register from './pages/Register/Register.vue';

import Gallery from './pages/Product/Gallery.vue';
import Description from './pages/Product/Description.vue';
import Measures from './pages/Product/Measures.vue';
import Shipping from './pages/Product/Shipping.vue';
import Social from './pages/Product/Social.vue';
import Buttons from './pages/Product/Buttons.vue';
import Name from './pages/Product/Name.vue';
import Skus from './pages/Product/Skus.vue';
import Price from './pages/Product/Price.vue';

import Filters from './pages/category/Filters.vue';
import Category from './pages/category/category.vue';

Vue.use(VueMask);

new Vue({
    el: '#app',
    store,
    components: {
        overlay,
        Search,
        Cart,
        Login,
        Favorites,
        Navigation,
        Shelf,
        Instagram,
        Newsletter,
        Register,
        Gallery,
        Description,
        Measures,
        Shipping,
        Social,
        Buttons,
        Name,
        Skus,
        Price,
        Filters,
        Category
    }
});