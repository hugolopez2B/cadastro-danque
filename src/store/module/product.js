
import vtex from '../../services/api';

const state = {
    'product': {},
    'skusCurrent': [],
    "PRODUCT_NAME": null,
    "PRODUCT_CODE_REFERENCE": null,
    "PRODUCT_IMAGEMS": null,
    "PRODUCT_DESCRIPTION": null,
    "PRODUCT_LIST_PRICE": null,
    "PRODUCT_BEST_PRICE": null,
    "TABLE_MEASUREMENT": null,
}

const getters = {
    SKUS : state => {
        return state.product.skus;
    },
    GET_SKUS_CURRENT : state => {
        return state.skusCurrent;
    },
    GET_NAME : state => {
        return state.PRODUCT_NAME;
    },
    GET_CODE_REFERENCE : state => {
        return state.PRODUCT_CODE_REFERENCE;
    },
    GET_IMAGEMS : state => {
        return state.PRODUCT_IMAGEMS;
    },
    GET_DESCRIPTION : state => {
        return state.PRODUCT_DESCRIPTION;
    },
    GET_LIST_PRICE : state => {
        return state.PRODUCT_LIST_PRICE;
    },
    GET_BEST_PRICE : state => {
        return state.PRODUCT_BEST_PRICE;
    },
    GET_TABLE_MEASUREMENT : state => {
        return state.TABLE_MEASUREMENT;
    }
}

const mutations = {
    SET_PRODUCT : (state, product) => {
        state.product = {
            'id': product.productId,
            'link': product.link,
            'categoryId': product.categoryId,
            'clusters': product.productClusters,
            'skus': product.items,
            'code': product.productReference,
            'Tabela de medidas': product['Tabela de medidas']
        };
    },
    SET_NAME : (state, NAME) => {
        state.PRODUCT_NAME = NAME;
    },
    SET_CODE_REFERENCE : (state, CODE_REFERENCE) => {
        state.PRODUCT_CODE_REFERENCE = CODE_REFERENCE;
    },
    SET_IMAGEMS : (state, IMAGES) => {
        state.PRODUCT_IMAGEMS = IMAGES.map((item) => {
            return {
                "sku": item.itemId,
                "image": item.images[0].imageTag
            }
        })
    },
    SET_DESCRIPTION : (state, DESCRIPTION) => {
        state.PRODUCT_DESCRIPTION = DESCRIPTION;
    },
    SET_BEST_PRICE : (state, BEST_PRICE) => {
        state.PRODUCT_BEST_PRICE = BEST_PRICE;
    },
    SET_LIST_PRICE : (state, LIST_PRICE) => {
        state.PRODUCT_LIST_PRICE = LIST_PRICE;
    },
    SET_TABLE_MEASUREMENT : (state, TABLE) => {
        state.TABLE_MEASUREMENT = TABLE;
    },
    SET_SKUS_CURRENT : (state, product) => {
        const productInState = state.skusCurrent.filter((item) => {
            return item.id == product.id;
        });

        if (!productInState.length && product.checked) {
            state.skusCurrent.push(product);
        }

        if (!product.checked) {
            const removeItem = state.skusCurrent.filter((item) => {
                return item.checked;
            });

            state.skusCurrent = removeItem;
        }
    },
}

const actions = {
    GET_PRODUCT : async ({ commit }) => {
        const { data } = await vtex.get(`/api/catalog_system/pub/products/search/?fq=productId:${skuJson_0.productId}`);
        
        commit("SET_PRODUCT", data[0]);

        commit("SET_NAME", data[0].productName);

        commit("SET_CODE_REFERENCE", data[0].productReference);

        commit("SET_IMAGEMS", data[0].items);

        commit("SET_DESCRIPTION", data[0].description);

        commit("SET_BEST_PRICE", data[0].items[0].sellers[0].commertialOffer.Price);

        commit("SET_LIST_PRICE", data[0].items[0].sellers[0].commertialOffer.ListPrice);

        commit("SET_TABLE_MEASUREMENT", data[0]['Tabela de medidas']);
    }
}  

export default {
    state, getters, mutations, actions
}