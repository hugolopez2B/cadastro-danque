import api from '../../services/vtex.js'

const state = {
    'loggedIn': null,
    'firstName': null
}

const getters = {
    GET_USER_IS_LOGGED: state => {
        return state.loggedIn;
    },
    GET_USER_NAME: state => {
        return state.firstName;
    }
}

const mutations = {
    SET_USER_IS_LOGGED : (state, user) => {
        state.loggedIn = user;
    },
    SET_USER_NAME : (state, firstName) => {
        state.firstName = firstName;
    }
}

const actions = {
    GET_USER : async ({ commit }) => {
        const { data } = await api.get('/no-cache/profileSystem/getProfile');
        
        commit('SET_USER_IS_LOGGED', data.IsUserDefined);

        commit('SET_USER_NAME', (data.IsUserDefined ? data.FirstName : null));
    }
}  

export default {
  state, getters, mutations, actions
}