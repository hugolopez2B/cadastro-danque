const state = {
    'login': false,
    'overlay': false
}

const getters = {
    GET_MODAL_LOGIN: state => {
        return state.login;
    }
}

const mutations = {
    SET_MODAL_LOGIN : (state, login) => {
        state.login = login;
    }
}

const actions = {
}  

export default {
  state, getters, mutations, actions
}