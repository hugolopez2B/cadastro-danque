import vtex from '../../services/api';

const state = {
    "products": [],
    'filters_selected': [],
    'order_by': null,
    "SET_URL_FILTERS": "",
    "SEARCH_EMPTY": false,
    "qtd_total": 0
}

const getters = {
    GET_PRODUCTS_CATEGORY : state => {
        return state.products;
    },
    GET_SEARCH_EMPTY : state => {
        return state.SEARCH_EMPTY;
    },
    FILTERS_SELECTEDS: state => {
        return state.filters_selected;
    }
}

const mutations = {
    SET_PRODUCTS_CATEGORY : (state, data) => {
        if (data.reset) {
            state.products = [];
        }   

        state.products = [...state.products, ...data.products];
    },
    SET_SEARCH_EMPTY: (state, PRODUCTS) => {       
        if(PRODUCTS.length>=12 || state.qtd_total > 12){             
            state.qtd_total = 12 + PRODUCTS.length;         
            state.SEARCH_EMPTY =  true;
        }
        else {            
            state.SEARCH_EMPTY =  false;
        }
    },
    SET_URL_FILTERS: (state, LINK) => {
        state.SET_URL_FILTERS = LINK;
    },
    SET_FILTERS_SELECTED: (state, filter) => {
        state.filters_selected = filter;
    },
    SET_ORDER_BY: (state, order) => {
        state.order_by = order;
    }
}

const actions = {
    FETCH_PRODUCTS_CATEGORY : async ({commit, state}, payload) => {
        const url = new URL(window.location.href);
        let urlSearch = "";

        if (url.searchParams.get("collection")) {
            urlSearch = `/api/catalog_system/pub/products/search?fq=H:${url.searchParams.get("collection")}&_from=${payload.from}&_to=${payload.to}`;
        } else {
            urlSearch = `/api/catalog_system/pub/products/search${location.pathname}?_from=${payload.from}&_to=${payload.to}${state.SET_URL_FILTERS}&O=${state.order_by}`;
        }

        const { data } = await vtex.get(urlSearch);

        commit("SET_PRODUCTS_CATEGORY", {
            'products': data,
            'reset': payload.reset
        });

        commit("SET_SEARCH_EMPTY", data);
    }
} 

export default {
    state, getters, mutations, actions
}