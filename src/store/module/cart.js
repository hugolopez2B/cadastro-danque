const state = {
    'showModal': false,
    'products': [],
    'quantity': null,
    'value': 0
}

const getters = {
    GET_SHOW_CART : state => {
        return state.showModal;
    },
    GET_PRODUCTS : state => {
        return state.products;
    },
    GET_VALUE_TOTAL: state => {
        return state.value;
    },
    GET_QUANTITY_PRODUCTS: state => {
        return state.quantity;
    }
}

const mutations = {
    SET_SHOW_CART : (state, modal) => {
        state.showModal = modal;
    },
    SET_VALUE_TOTAL : (state, value) => {
        state.value = (value / 100);
    },
    SET_PRODUCTS : (state, products) => {
        state.products = products.map((item) => {
            return {
                "skuId": item.id,
                "name": item.name,
                "price": ((item.price / 100) * item.quantity),
                "image": item.imageUrl,
                "link": item.detailUrl,
                "quantity": item.quantity
            }
        });
    },
    SET_QUANTITY_PRODUCTS : (state, products) => {
        state.quantity = products.reduce((total, item) => {
            return parseInt(total + item.quantity)
        },0);
    }
}

const actions = {
    GET_ORDER_FORM: ({ commit }) => {
        vtexjs.checkout.getOrderForm().done((orderForm) => {
            commit('SET_PRODUCTS', orderForm.items);
            commit('SET_QUANTITY_PRODUCTS', orderForm.items);
            commit('SET_VALUE_TOTAL', orderForm.value);
        });
    }
}  

export default {
    state, getters, mutations, actions
}