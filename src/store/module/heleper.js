const state = {
    'MODAL_LOGIN': false,
    'overlay': false
}

const getters = {
    GET_MODAL_LOGIN: state => {
        return state.MODAL_LOGIN;
    },
    GET_OVERLAY: state => {
        return state.overlay;
    }
}

const mutations = {
    SET_SHOW_MODAL_LOGIN : (state, LOGIN) => {
        state.MODAL_LOGIN = LOGIN;
    },
    SET_OVERLAY: (state, overlay) => {
        state.overlay = overlay;
    }
}

const actions = {
}  

export default {
  state, getters, mutations, actions
}