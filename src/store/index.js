import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user';
import product from './module/product';
import cart from './module/cart';
import category from './module/category';
import helper from './module/modal.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules : {
        user,
        helper,
        product,
        cart,
        category
    }
})