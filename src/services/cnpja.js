import axios from 'axios';

const cnpja = axios.create({
    baseURL: 'https://api.cnpja.com.br',
    headers: { 
        "authorization": "ab6e70d8-d2c5-4cc0-b169-2bf9f6fd2da4-335c1717-cec5-4524-9e9f-f1fc517d233c"
    },
});

export default cnpja;