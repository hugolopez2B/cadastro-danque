import axios from 'axios';

const instagram = axios.create({
    baseURL: 'https://api.instagram.com'
});

export default instagram;