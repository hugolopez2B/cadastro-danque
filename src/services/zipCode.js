import axios from 'axios';

const zipCode = axios.create({
    baseURL: 'https://viacep.com.br'
});

export default zipCode;