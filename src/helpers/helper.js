module.exports = {
    formatMoney (number, decimals, dec_point, thousands_sep, symbol) {
        if (!number || !decimals || !dec_point || !thousands_sep || !symbol) return;

        number = (number + '').replace(',', '').replace(' ', '');
        
        let n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                let k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };

        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }

        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }

        return symbol + " " + s.join(dec);
    },
    limitLetter (str, max_qtd) {
        if (!str || !max_qtd) return '';
        return  (str.length > max_qtd ? `${str.substring(0, max_qtd)} ...` : str.substring(0, max_qtd));
    }
}