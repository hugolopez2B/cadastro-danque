(function($, window, document) {
	var $win = $(window);
	var $doc = $(document);
	
	$doc.ready(function(){
        $('.helperComplement').remove();

        slideMain();
        slideProducts();

        $('.footer').on('click', '.footer__nav h5', function() {
            $(this).toggleClass('active');
            $(this).next().toggleClass('active');
        });
    });

    $win.on('load', function(){
    });
})(jQuery, window, document);

function slideMain() {
    var $slideMain = $('.section__slide' + (screen.width <= 767 ? '.device--mobile' : '.device--desktop'));

    $slideMain.find('.owl-carousel').owlCarousel({
        loop: true,
        nav: true,
        dots: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1
            }
        }
    })
}

function slideProducts() {
    $('.section__products .shelf > ul').addClass('owl-carousel').owlCarousel({
        loop: false,
        nav: true,
        mouseDrag: false,
        dots: false,
        slideBy: 1,
        autoplayTimeout: 3000,
        responsive:{
            0:{
                items: 1,
            },
            1000:{
                items: 4,
            }
        }
    });
}

$(".navigation__components .navigation ul li a").click(function(){
    $(this).toggleClass("active");
    $(this).next().toggleClass("active");
    });
    $(".widget.widget__specification .tabs").click(function(){
        $(this).toggleClass("active");
        });