const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const minifycss = require('gulp-cssmin');
const uglify = require('gulp-uglify');
const { parallel } = require('gulp');
const package = require('./package.json');

function css() {
    return gulp.src("./src/css/*.scss")
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(minifycss())
        .pipe(concat(package.name + '.app.min.css'))
        .pipe(gulp.dest('public/arquivos'));
}

function js() {
    return gulp.src("./src/js/*.js")
        .pipe(concat(package.name + '.app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/arquivos'))
}

function vendorJS() {
    return gulp.src("./src/vendor/*.js")
        .pipe(concat(package.name + '.vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/arquivos'))
}

function vendorCSS() {
    return gulp.src("./src/vendor/*.css")
        .pipe(minifycss())
        .pipe(concat(package.name + '.vendor.min.css'))
        .pipe(gulp.dest('public/arquivos'));
}

function watch() {
    gulp.watch("./src/css/*.scss", css);
    gulp.watch("./src/js/*.js", js);
    gulp.watch("./src/vendor/*.css", vendorCSS);
    gulp.watch("./src/vendor/*.js", vendorJS);
}

exports.watch = watch();
exports.default = parallel(css, js, vendorJS, vendorCSS);