const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin  = require('mini-css-extract-plugin');
const path = require('path');
const package = require('./package.json');


module.exports = {
    mode: 'production',
    entry: {
        "components": ['./src/app.js', './src/js/functions.js'],
    },
    output: {
        path: path.resolve(__dirname, 'public/assets'),
        filename: package.name + '.[name].mim.js',
        publicPath: '/public/pages'
    },
    resolve: {
        extensions: [
            '.js', 
            '.scss', 
            '.vue'
        ],
        alias: {
            'components': path.join(__dirname, 'src/components'),
            'vue': 'vue/dist/vue.common.js',
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: package.name + '.[name].mim.css',
            ignoreOrder: false, 
        }),
    ]
}